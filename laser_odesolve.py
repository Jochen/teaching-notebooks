from __future__ import division, print_function
import numpy as np
import matplotlib.pylab as plt
from scipy.integrate import odeint

def create_random_NRZOOK(t, reprate):
    rtmp = np.random.random_integers(0,1,
            size=np.ceil((t.max()-t.min())*reprate))
    field = np.ones(t.size, dtype=np.float)
    for i in range(len(rtmp)):
        #field[np.where((abs(abs(t)-(i+1)/reprate)<=1)&(abs(t)-
            #(i+1)/reprate>0))] *=rtmp[i]
        idx = np.where(abs(t*reprate-i)<=0.5)
        field[idx] *= rtmp[i]
    return field

#I = np.linspace(0,20,100)*1e-3
#N = np.zeros(len(I))
#S = np.zeros(len(I))
#phi = np.zeros(len(I))
#y0 = np.array([N,S,I])#.transpose()
#y0 = np.array([N,S,phi,I]).transpose()
#print(y0.shape)

def rk4(fct, y, t, dt):
    k1 = fct(y, t)
    k2 = fct(y+0.5*k1*dt, t+0.5*dt)
    k3 = fct(y+0.5*k2*dt, t+0.5*dt)
    k4 = fct(y+k3*dt, t+dt)
    return y + 1/6.*(k1+2*k2+2*k3+k4)*dt


gamma = 0.44
g0 = 3.e-6
N0 = 1.2e18
eps = 3.4e-17
tau_p = 1.e-12
beta = 4.e-4
#tau_n = 1.e-9
tau_n = 3.e-9
V = 9.e-11
q = 1.602e-19
eta = 0.1
hbar=6.624e-34
alpha=0.

def rateequations(state, time):
    fract = g0*(state[0]-N0)*state[1]/(1+eps*state[1])
    N = state[2]/(q*V)-fract-state[0]/tau_n
    S = gamma*fract-state[1]/tau_p+gamma*beta/tau_n*state[0]
    #phi = 0.5*alpha*(gamma*g0*(state[0]-N0)-1/tau_p)
    #return np.array([N, S, phi, state[3]])
    out= np.array([N, S, state[2]])
    return out

dt = 0.5e-12
#t = np.arange(0,1.e-7, dt)

#answer = odeint(rateequations, [0.,0.,9.5e-3], t)
#print(answer[-1])
#answer = []
#for i in range(len(I)):
    #y = y0[i]
    #print(y.shape)
    #answer.append(odeint(rateequations, y, t))
#answer = odeint(rateequations, y0, t)

#answer = np.array([0,0,10e-3])
Im = 30.e-3
Ith = 10.e-3
t = np.arange(0, 1e-7, dt)
#In = np.ones(len(t))*Ith
#In[np.where(abs(t-2.5e-9)<2e-9)] = Im
#N = np.zeros(len(In))
#S = np.zeros(len(In))
#y0 = np.array([N,S,In])
I = create_random_NRZOOK(t, 5.e9)
I = I*Im+Ith
answer = []
tmp= np.array([0,0,0])
for i in range(len(t)):
    tmp[2] = I[i]
    tmp = rk4(rateequations, tmp, t[i], dt)
    answer.append(tmp)


#answer2 = odeint(rateequations, np.array([0,0,10e-3]), t)

answer=np.array(answer)
#print(a)
#print(answer2)
f = 3.e8/1550.e-9
#pout = a[:,-1,1]*V*eta*hbar*f/(2*gamma*tau_p)*1e3
pout = answer[:,1]*V*eta*hbar*f/(2*gamma*tau_p)*1e3

#pout_las = pout[np.where(I>0.013)]
#I_las = I[np.where(I>0.013)]
#p = np.polyfit(I_las, pout_las, 1)
#print(-p[1]/p[0])
plt.plot(t, pout)
plt.plot(t, I, 'r')
#plt.plot(I, p[0]*I+p[1], 'r')
plt.show()

