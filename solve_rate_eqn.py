import numpy as np
q = 1.602e-19

def _rateequations(state, time, pmtrs):
    #, g0=3e-6, N0=1.2e18, eps=3.4e-17, V=9e-11, tau_n=3e-9, gamma=0.44, beta=4e-4, alpha=0, tau_p=1e-12):
    out = np.zeros_like(state)
    #based on L. Bjerkan in JLT 14 N 5, 839 (1996)
    fract = pmtrs['g0']*(state[0]-pmtrs['N0'])*state[1]/(1+pmtrs['eps']*state[1])
    out[0] = state[3]/(q*pmtrs['V'])-fract-state[0]/pmtrs['tau_n']
    out[1] = pmtrs['gamma']*fract-state[1]/pmtrs['tau_p']+pmtrs['gamma']*pmtrs['beta']/pmtrs['tau_n']*state[0]
    out[2] = 0.5*pmtrs['alpha']*(pmtrs['gamma']*pmtrs['g0']*(state[0]-pmtrs['N0'])-1/pmtrs['tau_p'])
    out[3] = state[3]
    #return np.array([N, S, phi, state[3]])
    return out

def rk4(fct, y, t, dt, pmtrs):
    k1 = fct(y, t, pmtrs)
    k2 = fct(y+0.5*k1*dt, t+0.5*dt, pmtrs)
    k3 = fct(y+0.5*k2*dt, t+0.5*dt, pmtrs)
    k4 = fct(y+k3*dt, t+dt, pmtrs)
    return y + 1/6.*(k1+2*k2+2*k3+k4)*dt
#def cal_threshold(Imax, npts, t, dt, g0=3e-6, N0=1.2e18, eps=3.4e-17, V=9e-11, tau_n=3e-9, gamma=0.44, beta=4e-4, alpha=0, tau_p=1e-12):

#pythran export cal_threshold(float64[], float64[], float64, float64 , float64, float64, float64, float64, float64, float64, float64, float64 ))
def cal_threshold(I, t, dt, g0, N0, eps, V, tau_n, gamma, beta, alpha, tau_p):
    N = np.zeros_like(I)
    S = np.zeros_like(I)
    phi = np.zeros_like(I)
    y0 = np.array([N, S, phi, I])
    prmtrs = {"g0": g0, "N0":N0, "eps":eps, "V":V, "tau_n":tau_n, "gamma":gamma, "beta":beta, "alpha":alpha, "tau_p":tau_p}
    #y0 = np.zeros((4, npts), dtype=np.float64)
    #y0[0] = N
    #y0[1] = S
    #y0[2] = phi
    #y0[3] = I
    #answer = (N, S, phi, I)
    answer = y0
    for i in range(len(t)):
        answer = rk4(_rateequations, answer, t[i], dt, prmtrs)
    return answer
    

#pythran export cal_data(float64[], float64[], float64, float64 , float64, float64, float64, float64, float64, float64, float64, float64 ))
def cal_data(I, t, dt, g0, N0, eps, V, tau_n, gamma, beta, alpha, tau_p):
    x0 = np.zeros(4, dtype=np.float64)
    prmtrs = {"g0": g0, "N0":N0, "eps":eps, "V":V, "tau_n":tau_n, "gamma":gamma, "beta":beta, "alpha":alpha, "tau_p":tau_p}
    answer = []
    for i in range(t.size):
        x0[3] = I[i]
        x0 = rk4(_rateequations, x0, t[i], dt, prmtrs)
        answer.append(x0)
    return np.array(answer)
        


